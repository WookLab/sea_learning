/* Program that goes through a series of array properties in C
 *
 * AUTHOR: William Cook
 *   DATE: 13/07/19
 */

#include <stdio.h>	/* prinf, getchar */


#define LEN_ONE 10	/* length of first array */


void printArray(int *array, int printLen); 


int main () {
	int arrayOne[10];

	printf("\n\t- Array properties -\n");

	printf("Contents of arrayOne before initialising values:\n");
	printArray(arrayOne, LEN_ONE);


	for (int i = 0; i < LEN_ONE; i++) 	/* populate array */
	   	arrayOne[i] = i;
	

	printf("\n");

	printf("Contents of arrayOne after initialising values:\n");
	printArray(arrayOne, LEN_ONE);

	getchar();	/* wait for user input before closing program */

	return 0;
}


/* printArray: prints the contents of array until printLen */
void printArray(int *array, int printLen) {
	int i;

	for (i = 0; i <= printLen-2; i++) {	/* print contents exclusing last */
		printf("[%d] ", i);
		printf("%d, ", *(array+i));
	}	

	printf("[%d] ", i);
	printf("%d\n", *(array+i));	/* print last value */
}
